package ligma

import (
	"chem-stock/internal/ghs"
	"encoding/json"
	"github.com/antchfx/htmlquery"
	"github.com/valyala/fasthttp"
	"log"
	"strings"
)

type data struct {
	Name        string
	CAS         string
	MW          string
	Signalword  string
	Pictograms  []string
	Hcodes      []string
	Hstatements string
	Pcodes      []string
	Pstatements string
}

func ScrapeController(ctx *fasthttp.RequestCtx) {
	url := string(ctx.QueryArgs().Peek("url"))
	output := scrape(url)
	outputs, _ := json.Marshal(output)
	ctx.SetContentType("application/json")
	ctx.Write(outputs)
}

func scrape(url string) (output data) {
	doc, err := htmlquery.LoadURL(url)
	if err != nil {
		return data{}
	}

	namesection, err := htmlquery.QueryAll(doc, "//span[@id='product-name']")
	if err != nil {
		log.Println(err.Error())
	} else {
		if len(namesection) > 0 {
			output.Name = htmlquery.InnerText(namesection[0])
		}
	}

	mwsection, err := htmlquery.QueryAll(doc, "//div[contains(text(), 'Molecular Weight: ')]/../following-sibling::div")
	if err != nil {
		log.Println(err.Error())
	} else {
		if len(mwsection) > 0 {
			output.MW = htmlquery.InnerText(mwsection[0])
		}
	}

	cassection, err := htmlquery.QueryAll(doc, "//div[contains(text(), 'CAS Number: ')]/../following-sibling::div")
	if err != nil {
		log.Println(err.Error())
	} else {
		if len(cassection) > 0 {
			output.CAS = htmlquery.InnerText(cassection[0])
		}
	}

	picsection, err := htmlquery.QueryAll(doc, "//a[contains(@href, '#pictogram')]")
	if err != nil {
		log.Println(err.Error())
	} else {
		if len(picsection) > 0 {
			output.Pictograms = strings.Split(htmlquery.InnerText(picsection[0]), ",")
		}
	}

	signalwordsection, err := htmlquery.QueryAll(doc, "//h3[contains(text(), 'Signal Word')]/following-sibling::div")
	if err != nil {
		log.Println(err.Error())
	} else {
		if len(signalwordsection) > 0 {
			output.Signalword = htmlquery.InnerText(signalwordsection[0])
		}
	}

	hsection, err := htmlquery.QueryAll(doc, "//a[contains(@href, '#hazard')]")
	if err != nil {
		log.Println(err.Error())
	} else {
		if len(hsection) > 0 {
			output.Hcodes = ghs.ParseHCodes(htmlquery.InnerText(hsection[0]))
			output.Hstatements = ghs.DecodeHWords(output.Hcodes)
		}
	}

	psection, err := htmlquery.QueryAll(doc, "//a[contains(@href, '#precautionary')]")
	if err != nil {
		log.Println(err.Error())
	} else {
		if len(psection) > 0 {
			output.Pcodes = ghs.ParsePCodes(htmlquery.InnerText(psection[0]))
			output.Pstatements = ghs.DecodePWords(output.Pcodes)
		}
	}

	return
}
