package ligma

import (
	"encoding/json"
	"fmt"
	"github.com/antchfx/htmlquery"
	"github.com/valyala/fasthttp"
	"golang.org/x/net/html"
	"net/url"
)

type product struct {
	Number      string
	Url         string
	Description string
}

type substance struct {
	Name     string
	CAS      string
	Products []product
}

func SearchName(ctx *fasthttp.RequestCtx) {
	query := url.QueryEscape(string(ctx.QueryArgs().Peek("q")))
	doc, err := htmlquery.LoadURL(fmt.Sprintf("https://www.sigmaaldrich.com/EE/en/search/%s?facet=facet_brand%%3ASigma-Aldrich&focus=products&page=1&perPage=30&region=global&sort=relevance&term=%s&type=product", query, query))
	if err != nil {
		return
	}
	data := parsedoc(doc)

	out, _ := json.Marshal(data)
	ctx.SetContentType("application/json")
	ctx.Write(out)
}
func parsedoc(node *html.Node) (out []substance) {
	nodes, _ := htmlquery.QueryAll(node, "//div[@data-testid='srp-substance-group']")

	for _, v := range nodes {
		subs := substance{}

		name, err := htmlquery.QueryAll(v, "//div[contains(@class,'MuiGrid-grid-xs-12')]/div[contains(@class,'MuiGrid-grid-xs-12')]/*/*")
		if err == nil && len(name) > 0 {
			subs.Name = htmlquery.InnerText(name[0])
		}

		cas, err := htmlquery.QueryAll(v, "//div[contains(text(), 'CAS Number')]/following-sibling::a")
		if err == nil && len(cas) > 0 {
			subs.CAS = htmlquery.InnerText(cas[0])
		}

		productRows, err := htmlquery.QueryAll(v, "//tbody/tr")

		if err == nil {
			for _, row := range productRows {
				prod := product{}
				td, err := htmlquery.QueryAll(row, "//td")
				if err == nil && len(td) > 0 {
					a, err := htmlquery.QueryAll(td[0], "/a")
					if err == nil && len(a) > 0 {
						prod.Url = htmlquery.SelectAttr(a[0], "href")
						prod.Number = htmlquery.InnerText(a[0])
					}
					prod.Description = htmlquery.InnerText(td[1])
					subs.Products = append(subs.Products, prod)
				}
			}
		}
		out = append(out, subs)
	}
	return
}
