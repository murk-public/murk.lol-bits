package ghs

import (
	"strings"
)

func GetHText(codes string) string {
	return DecodeHWords(ParseHCodes(codes))
}

func GetPText(codes string) string {
	return DecodePWords(ParsePCodes(codes))
}

func ParseHCodes(input string) []string {
	return strings.Split(input, " - ")
}

func DecodeHWords(hcodes []string) string {
	var words []string
	for _, code := range hcodes {
		if val, ok := hazardStatements[code]; ok {
			words = append(words, val)
		}
	}
	return strings.Join(words, " ")
}

func ParsePCodes(input string) []string {
	return strings.Split(input, " - ")
}

func DecodePWords(pcodes []string) string {
	var words []string
	for _, code := range pcodes {
		if val, ok := precautionaryStatements[code]; ok {
			words = append(words, val)
		}
	}
	return strings.Join(words, " ")
}
