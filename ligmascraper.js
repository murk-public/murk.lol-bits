$(document).ready(function () {
    $(".copybut").click(function () {
        $(this).next().select();
        document.execCommand("copy");
    });

})

function search() {
    $.ajax({
        datatype: "json",
        url: "/api/ligma/search/name",
        data: {
            q: $("#searchTerm").val()
        },
        success: function (data) {
            if (data == []) {
                $("#searchResults").html('<li class="collection-item">No results</li>');
                return;
            }
            $("#searchResults").html("");
            data.forEach(function (result) {
                $("#searchResults").append(resultHTML(result));
                console.log(result);
            })
        }
    })
}

function scrape() {
    $.ajax({
        datatype: "json",
        url: "/api/ligma/scrape",
        data: {
            url: $("#url").val()
        },
        success: function (data) {
            $("#chemName").html(data.Name)
            $("#CAS").html(data.CAS)
            $("#MW").html(data.MW)
            $("#Signalword").html(data.Signalword)
            $("#Pstatements").html(data.Pstatements)
            $("#Pstatements").prev().val(data.Pstatements)
            $("#Hstatements").html(data.Hstatements)
            $("#Hstatements").prev().val(data.Hstatements)

            $("#Hcodes").html("");
            data.Hcodes.forEach(function (value) {
                $("#Hcodes").append("<li>" + value + "</li>");
            })

            $("#Pcodes").html("");
            data.Pcodes.forEach(function (value) {
                $("#Pcodes").append("<li>" + value + "</li>");
            })

            $("#pictograms").html("");
            data.Pictograms.forEach(function (value) {
                $("#pictograms").append(pictogramHTML(value));
            })

            $("#result").removeClass("hide");
        }
    });
}

function pictogramHTML(code) {
    return '<div class="col s4 m3 l2 pictogram"><img class="responsive-img" src="/web/img/pictograms/' + code + '.svg"/></div>'
}

function resultHTML(result) {
    let products = '<li class="collection-header"><h5>' + result.Name + '</h5><h6>CAS: ' + result.CAS + '</h6></li>';
    result.Products.forEach(function (p) {
        products = products +
            '<li class="collection-item row"><div class="col s3 fat">' +
            p.Number + ' </div> <span class="center">' + p.Description +
            '</span><a href="#!" class="modal-close secondary-content deep-purple-text darken-2" aria-data="https://www.sigmaaldrich.com' +
            p.Url +
            '" onclick="useURL(this)"><i class="material-icons">send</i></a></li>';
    })
    return products;
}

function useURL(t) {
    let surl = t.getAttribute("aria-data");
    console.log(surl);
    $("#url").val(surl);
    scrape();
}